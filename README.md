# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Proof of Tableau Web Connector API using College Scorecard Data located at data.gov
* 0.1

# NOTE: CURRENTLY THIS CODE DOESN"T WORK FOR TABLEAU DASHBOARD #
### How do I get set up? ###

* Clone the repo
* Run a local server from the directory where you have cloned the repo
* Open Tableau
* From Data Sources, choose Data Connector
* In the URL column enter http://localhost:<port number>

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jay Parchure- jay[dot]parchure[at]act.org
* Other community or team contact